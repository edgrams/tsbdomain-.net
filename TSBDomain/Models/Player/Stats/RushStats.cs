﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSBDomain.Models.Player.Stats
{
    public class RushStats
    {
        /// <summary>
        /// Creates the specified attempts.
        /// </summary>
        /// <param name="attempts">The attempts.</param>
        /// <param name="yards">The yards.</param>
        /// <param name="touchdowns">The touchdowns.</param>
        /// <returns></returns>
        public static RushStats Create(byte attempts, Int16 yards, byte touchdowns)
        {
            return new RushStats
            {
                Attempts = attempts,
                Yards = yards,
                Touchdowns = touchdowns,
            };
        }

        /// <summary>
        /// Gets or sets the attempts.
        /// </summary>
        /// <value>
        /// The attempts.
        /// </value>
        public byte Attempts { get; set; }

        /// <summary>
        /// Gets or sets the yards.
        /// </summary>
        /// <value>
        /// The yards.
        /// </value>
        public Int16 Yards { get; set; }

        /// <summary>
        /// Gets or sets the touchdowns.
        /// </summary>
        /// <value>
        /// The touchdowns.
        /// </value>
        public byte Touchdowns { get; set; }
    }
}
