﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSBDomain.Models.Player.Stats
{
    public class KickStats
    {
        /// <summary>
        /// Creates the specified extra points made.
        /// </summary>
        /// <param name="extraPointsMade">The extra points made.</param>
        /// <param name="extraPointAttempts">The extra point attempts.</param>
        /// <param name="fieldGoalsMade">The field goals made.</param>
        /// <param name="fieldGoalAttempts">The field goal attempts.</param>
        /// <returns></returns>
        public static KickStats Create(byte extraPointsMade, byte extraPointAttempts,
            byte fieldGoalsMade, byte fieldGoalAttempts)
        {
            return new KickStats
            {
                ExtraPointsMade = extraPointsMade,
                ExtraPointAttempts = extraPointAttempts,
                FieldGoalsMade = fieldGoalsMade,
                FieldGoalAttempts = fieldGoalAttempts
            };
        }

        /// <summary>
        /// Gets or sets the extra points made.
        /// </summary>
        /// <value>
        /// The extra points made.
        /// </value>
        public byte ExtraPointsMade { get; set; }

        /// <summary>
        /// Gets or sets the extra point attempts.
        /// </summary>
        /// <value>
        /// The extra point attempts.
        /// </value>
        public byte ExtraPointAttempts { get; set; }

        /// <summary>
        /// Gets or sets the field goals made.
        /// </summary>
        /// <value>
        /// The field goals made.
        /// </value>
        public byte FieldGoalsMade { get; set; }

        /// <summary>
        /// Gets or sets the field goal attempts.
        /// </summary>
        /// <value>
        /// The field goal attempts.
        /// </value>
        public byte FieldGoalAttempts { get; set; }
    }
}
