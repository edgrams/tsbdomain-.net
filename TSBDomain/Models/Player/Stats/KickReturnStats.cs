﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSBDomain.Models.Player.Stats
{
    public class KickReturnStats
    {
        /// <summary>
        /// Creates the specified returns.
        /// </summary>
        /// <param name="returns">The returns.</param>
        /// <param name="yards">The yards.</param>
        /// <param name="touchdowns">The touchdowns.</param>
        /// <returns></returns>
        public static KickReturnStats Create(byte returns, Int16 yards, byte touchdowns)
        {
            return new KickReturnStats
            {
                Returns = returns,
                Yards = yards,
                Touchdowns = touchdowns
            };
        }

        /// <summary>
        /// Gets or sets the returns.
        /// </summary>
        /// <value>
        /// The returns.
        /// </value>
        public byte Returns { get; set; }

        /// <summary>
        /// Gets or sets the yards.
        /// </summary>
        /// <value>
        /// The yards.
        /// </value>
        public Int16 Yards { get; set; }

        /// <summary>
        /// Gets or sets the touchdowns.
        /// </summary>
        /// <value>
        /// The touchdowns.
        /// </value>
        public byte Touchdowns { get; set; }
    }
}
