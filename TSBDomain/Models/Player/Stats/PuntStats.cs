﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSBDomain.Models.Player.Stats
{
    public class PuntStats
    {
        /// <summary>
        /// Creates the specified punts.
        /// </summary>
        /// <param name="punts">The punts.</param>
        /// <param name="yards">The yards.</param>
        /// <returns></returns>
        public static PuntStats Create(byte punts, Int16 yards)
        {
            return new PuntStats
            {
                Punts = punts,
                Yards = yards
            };
        }

        /// <summary>
        /// Gets the punts.
        /// </summary>
        /// <value>
        /// The punts.
        /// </value>
        public byte Punts { get; set; }

        /// <summary>
        /// Gets or sets the yards.
        /// </summary>
        /// <value>
        /// The yards.
        /// </value>
        public Int16 Yards { get; set; }
    }
}
