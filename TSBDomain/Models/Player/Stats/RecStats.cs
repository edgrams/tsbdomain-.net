﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSBDomain.Models.Player.Stats
{
    public class RecStats
    {
        /// <summary>
        /// Creates the specified attempts.
        /// </summary>
        /// <param name="receptions">The receptions.</param>
        /// <param name="yards">The yards.</param>
        /// <param name="touchdowns">The touchdowns.</param>
        /// <returns></returns>
        public static RecStats Create(byte receptions, Int16 yards, byte touchdowns)
        {
            return new RecStats
            {
                Receptions = receptions,
                Yards = yards,
                Touchdowns = touchdowns
            };
        }

        /// <summary>
        /// Gets or sets the receptions.
        /// </summary>
        /// <value>
        /// The receptions.
        /// </value>
        public byte Receptions { get; set; }

        /// <summary>
        /// Gets or sets the yards.
        /// </summary>
        /// <value>
        /// The yards.
        /// </value>
        public Int16 Yards { get; set; }

        /// <summary>
        /// Gets or sets the touchdowns.
        /// </summary>
        /// <value>
        /// The touchdowns.
        /// </value>
        public byte Touchdowns { get; set; }
    }
}
