﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSBDomain.Models.Player.Stats
{
    public class PassStats
    {
        /// <summary>
        /// Creates the specified attempts.
        /// </summary>
        /// <param name="attempts">The attempts.</param>
        /// <param name="completions">The completions.</param>
        /// <param name="yards">The yards.</param>
        /// <param name="touchdowns">The touchdowns.</param>
        /// <param name="interceptions">The interceptions.</param>
        /// <returns></returns>
        public static PassStats Create(byte attempts, byte completions, Int16 yards,
            byte touchdowns, byte interceptions)
        {
            return new PassStats
            {
                Attempts = attempts,
                Completions = completions,
                Yards = yards,
                Touchdowns = touchdowns,
                Interceptions = interceptions
            };
        }

        /// <summary>
        /// Gets or sets the attempts.
        /// </summary>
        /// <value>
        /// The attempts.
        /// </value>
        public byte Attempts { get; set; }

        /// <summary>
        /// Gets or sets the completions.
        /// </summary>
        /// <value>
        /// The completions.
        /// </value>
        public byte Completions { get; set; }

        /// <summary>
        /// Gets or sets the yards.
        /// </summary>
        /// <value>
        /// The yards.
        /// </value>
        public Int16 Yards { get; set; }

        /// <summary>
        /// Gets or sets the touchdowns.
        /// </summary>
        /// <value>
        /// The touchdowns.
        /// </value>
        public byte Touchdowns { get; set; }

        /// <summary>
        /// Gets or sets the interceptions.
        /// </summary>
        /// <value>
        /// The interceptions.
        /// </value>
        public byte Interceptions { get; set; }
    }
}
