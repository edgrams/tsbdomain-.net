﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using TSBDomain.Enums;
using TSBDomain.Models.Player.Stats;

namespace TSBDomain.Models.Team.Stats
{
    public class TeamStats
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TeamStats"/> class.
        /// </summary>
        public TeamStats()
        {
            this.PlayerStats = new List<PlayerStats>();
        }

        /// <summary>
        /// Creates the specified home team identifier.
        /// </summary>
        /// <param name="isHome">if set to <c>true</c> [is home].</param>
        /// <returns></returns>
        public static TeamStats Create(bool isHome)
        {
            return new TeamStats
            {
                IsHome = isHome
            };
        }

        /// <summary>
        /// Gets the game identifier.
        /// </summary>
        /// <value>
        /// The game identifier.
        /// </value>
        public int GameId { get; set; }

        /// <summary>
        /// Gets the team identifier.
        /// </summary>
        /// <value>
        /// The team identifier.
        /// </value>
        public byte TeamId { get; set; }

        /// <summary>
        /// Gets the opposing team identifier.
        /// </summary>
        /// <value>
        /// The opposing team identifier.
        /// </value>
        public byte OpposingTeamId { get; set; }

        /// <summary>
        /// Gets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        public Int16 UserId { get; set; }

        /// <summary>
        /// Gets the opponent user identifier.
        /// </summary>
        /// <value>
        /// The opponent user identifier.
        /// </value>
        public Int16 OpposingUserId { get; set; }

        /// <summary>
        /// Gets the decision.
        /// </summary>
        /// <value>
        /// The decision.
        /// </value>
        public Decision Decision { get; set; }

        /// <summary>
        /// Gets the first downs.
        /// </summary>
        /// <value>
        /// The first downs.
        /// </value>
        public byte FirstDowns { get; set; }

        /// <summary>
        /// Gets a value indicating whether [is home].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is home]; otherwise, <c>false</c>.
        /// </value>
        public bool IsHome { get; set; }

        /// <summary>
        /// Gets a value indicating whether [is overtime].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is overtime]; otherwise, <c>false</c>.
        /// </value>
        public bool IsOvertime { get; set; }

        /// <summary>
        /// Gets the score first quarter.
        /// </summary>
        /// <value>
        /// The score first quarter.
        /// </value>
        public double ScoreFirstQuarter { get; set; }

        /// <summary>
        /// Gets the score second quarter.
        /// </summary>
        /// <value>
        /// The score second quarter.
        /// </value>
        public double ScoreSecondQuarter { get; set; }

        /// <summary>
        /// Gets the score third quarter.
        /// </summary>
        /// <value>
        /// The score third quarter.
        /// </value>
        public double ScoreThirdQuarter { get; set; }

        /// <summary>
        /// Gets the score fourth quarter.
        /// </summary>
        /// <value>
        /// The score fourth quarter.
        /// </value>
        public double ScoreFourthQuarter { get; set; }

        /// <summary>
        /// Gets or sets the score overtime quarter.
        /// </summary>
        /// <value>
        /// The score overtime quarter.
        /// </value>
        public byte ScoreOvertimeQuarter { get; set; }

        /// <summary>
        /// Gets the score final.
        /// </summary>
        /// <value>
        /// The score final.
        /// </value>
        public byte ScoreFinal { get; set; }

        /// <summary>
        /// Gets the weather.
        /// </summary>
        /// <value>
        /// The weather.
        /// </value>
        public Weather Weather { get; set; }

        /// <summary>
        /// Gets the player stats.
        /// </summary>
        /// <value>
        /// The player stats.
        /// </value>
        public virtual ICollection<PlayerStats> PlayerStats { get; set; }
    }
}
