﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TSBDomain.Models.Team.Stats;

namespace TSBDomain.Models
{
    public class Game
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Game"/> class.
        /// </summary>
        public Game()
        {
            this.TeamStats = new List<TeamStats>();
        }

        /// <summary>
        /// Creates the specified team stats.
        /// </summary>
        /// <param name="teamStats">The team stats.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">teamStats</exception>
        public static Game Create(ICollection<TeamStats> teamStats)
        {
            if (teamStats == null)
            {
                throw new ArgumentNullException("teamStats");
            }

            return new Game
            {
                TeamStats = teamStats
            };
        }

        /// <summary>
        /// Gets the game identifier.
        /// </summary>
        /// <value>
        /// The game identifier.
        /// </value>
        public int GameId { get; set; }

        /// <summary>
        /// Gets or sets the upload timestamp.
        /// </summary>
        /// <value>
        /// The upload timestamp.
        /// </value>
        public DateTime UploadTimestamp { get; set; }

        /// <summary>
        /// Gets the upload user identifier.
        /// </summary>
        /// <value>
        /// The upload user identifier.
        /// </value>
        public Int16 UploadUserId { get; set; }

        /// <summary>
        /// Gets the team stats.
        /// </summary>
        /// <value>
        /// The team stats.
        /// </value>
        public virtual ICollection<TeamStats> TeamStats { get; set; }
    }
}
