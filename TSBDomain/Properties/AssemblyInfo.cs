﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("TSBDomain")]
[assembly: AssemblyDescription("TSBDomain")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("8BitRivals")]
[assembly: AssemblyProduct("TSBDomain")]
[assembly: AssemblyCopyright("Copyright ©  2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("47be5318-ba92-43c5-9ea3-2baeb70a64a2")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.1")]
[assembly: AssemblyFileVersion("1.0.0.1")]

// for rhino mocks to not make the interfaces public
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]

// friend assemblies
[assembly: InternalsVisibleTo("TSB.Rest")]
[assembly: InternalsVisibleTo("TSBDomain.Tests")]
[assembly: InternalsVisibleTo("TSBStatExtractor")]
[assembly: InternalsVisibleTo("TSBStatExtractor.Tests")]
