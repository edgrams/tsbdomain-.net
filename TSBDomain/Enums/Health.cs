﻿
namespace TSBDomain.Enums
{
    public enum Health : byte
    {
        Healthy,
        Probable,
        Questionable,
        Doubtful
    }
}
