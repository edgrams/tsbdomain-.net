﻿
namespace TSBDomain.Enums
{
    public enum Decision : byte
    {
        Win,
        Loss,
        Tie
    }
}
