﻿
namespace TSBDomain.Enums
{
    public enum Weather : byte
    {
        Fair,
        Rain,
        Snow
    }
}
