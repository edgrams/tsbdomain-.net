﻿
namespace TSBDomain.Enums
{
    public enum Emulator : byte
    {
        Nesticle,
        Nestopia,
        Unknown
    }
}
