﻿
namespace TSBDomain.Enums
{
    public enum Condition : byte
    {
        Bad,
        Average,
        Good,
        Excellent
    }
}
