﻿using System;

namespace TSBDomain.Exceptions
{
    public class TSBException : Exception
    {
        public TSBException(string message)
            : base(message) { }
    }
}
