﻿using NUnit.Framework;
using Rhino.Mocks;
using System;
using System.Collections.Generic;
using TSBDomain.Models;
using TSBDomain.Models.Team.Stats;

namespace TSBDomain.Tests.Models
{
    [TestFixture]
    class GameTests
    {
        private ICollection<TeamStats> teamStats;

        [SetUp]
        public void SetUp()
        {
            this.teamStats = MockRepository.GenerateMock<IList<TeamStats>>();
        }

        [Test]
        public void Create_Should_Throw_If_TeamStats_Is_Null()
        {
            Assert.Throws<ArgumentNullException>(() => Game.Create(null));
        }

        [Test]
        public void Create_Should_Return_Instance_Of_Game()
        {
            var result = Game.Create(this.teamStats);

            Assert.IsInstanceOf<Game>(result);
        }
    }
}
