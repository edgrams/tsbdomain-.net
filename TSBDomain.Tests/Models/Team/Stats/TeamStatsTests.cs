﻿using NUnit.Framework;
using TSBDomain.Models.Team.Stats;

namespace TSBDomain.Tests.Models.Team.Stats
{
    [TestFixture]
    class TeamStatsTests
    {
        [Test]
        public void Create_Should_Return_Instance_Of_TeamStats()
        {
            var result = TeamStats.Create(false);

            Assert.IsInstanceOf<TeamStats>(result);
        }
    }
}
