﻿using NUnit.Framework;
using TSBDomain.Models.Player.Stats;

namespace TSBDomain.Tests.Models.Player.Stats
{
    [TestFixture]
    class RecStatsTests
    {
        [Test]
        public void Create_Should_Return_Instance_Of_RecStats()
        {
            var result = RecStats.Create(0, 0, 0);

            Assert.IsInstanceOf<RecStats>(result);
        }
    }
}
