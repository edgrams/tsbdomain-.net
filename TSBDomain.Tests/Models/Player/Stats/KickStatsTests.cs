﻿using NUnit.Framework;
using TSBDomain.Models.Player.Stats;

namespace TSBDomain.Tests.Models.Player.Stats
{
    [TestFixture]
    class KickStatsTests
    {
        [Test]
        public void Create_Should_Return_Instance_Of_KickStats()
        {
            var result = KickStats.Create(0, 0, 0, 0);

            Assert.IsInstanceOf<KickStats>(result);
        }
    }
}
