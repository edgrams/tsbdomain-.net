﻿using NUnit.Framework;
using TSBDomain.Models.Player.Stats;

namespace TSBDomain.Tests.Models.Player.Stats
{
    [TestFixture]
    class PassStatsTests
    {
        [Test]
        public void Create_Should_Return_Instance_Of_PassStats()
        {
            var result = PassStats.Create(0, 0, 0, 0, 0);

            Assert.IsInstanceOf<PassStats>(result);
        }
    }
}
