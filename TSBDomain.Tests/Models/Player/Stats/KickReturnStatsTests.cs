﻿using NUnit.Framework;
using TSBDomain.Models.Player.Stats;

namespace TSBDomain.Tests.Models.Player.Stats
{
    [TestFixture]
    class KickReturnStatsTests
    {
        [Test]
        public void Create_Should_Return_Instance_Of_KickReturnStats()
        {
            var result = KickReturnStats.Create(0, 0, 0);

            Assert.IsInstanceOf<KickReturnStats>(result);
        }
    }
}
