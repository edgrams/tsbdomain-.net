﻿using NUnit.Framework;
using TSBDomain.Models.Player.Stats;

namespace TSBDomain.Tests.Models.Player.Stats
{
    [TestFixture]
    class RushStatsTests
    {
        [Test]
        public void Create_Should_Return_Instance_Of_RushStats()
        {
            var result = RushStats.Create(0, 0, 0);

            Assert.IsInstanceOf<RushStats>(result);
        }
    }
}
