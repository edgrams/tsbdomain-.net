﻿using NUnit.Framework;
using TSBDomain.Models.Player.Stats;

namespace TSBDomain.Tests.Models.Player.Stats
{
    [TestFixture]
    class PuntStatsTests
    {
        [Test]
        public void Create_Should_Return_Instance_Of_PuntStats()
        {
            var result = PuntStats.Create(0, 0);

            Assert.IsInstanceOf<PuntStats>(result);
        }
    }
}
