﻿using NUnit.Framework;
using TSBDomain.Models.Player.Stats;

namespace TSBDomain.Tests.Models.Player.Stats
{
    [TestFixture]
    class DefStatsTests
    {
        [Test]
        public void Create_Should_Return_Instance_Of_DefStats()
        {
            var result = DefStats.Create(0, 0, 0, 0);

            Assert.IsInstanceOf<DefStats>(result);
        }
    }
}
